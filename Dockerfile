FROM golang:1.15-alpine as builder

WORKDIR /go/src/message-api

RUN apk update && apk add ca-certificates && \
    apk add git && apk add gcc && apk add libc-dev \
    && rm -rf /var/cache/apk/*

COPY src .
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -o main


FROM scratch as prod
WORKDIR /
COPY --from=builder /etc/ssl/certs/ca-certificates.crt \
  /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /go/src/message-api/main .
CMD ["/main"]
