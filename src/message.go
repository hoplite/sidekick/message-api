package main

import (
	"fmt"

	"github.com/go-playground/validator/v10"
	"github.com/gofrs/uuid"
)

type Message struct {
	Id           string `json:"id"`
	Service      string `json:"service" validate:"required"`
	RoomId       string `json:"roomid" validate:"required"`
	MessageText  string `json:"messagetext,omitempty"`
	MarkdownText string `json:"markdowntext,omitempty"`
}

func NewMessage() *Message {
	id, _ := uuid.NewV4()
	m := &Message{Id: id.String()}

	return m
}

func (m *Message) Validate() error {
	var validate *validator.Validate
	validate = validator.New()
	// returns nil or ValidationErrors ( []FieldError )
	err := validate.Struct(m)
	if err != nil {
		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println(err)
		}

		// from here you can create your own error messages in whatever language you wish
		return err
	}
	return nil
}
