// main_test.go

package main_test

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gofrs/uuid"
	gomock "github.com/golang/mock/gomock"
	main "gitlab.com/hoplite/samwise/message-api"
)

var a main.App

func TestMain(m *testing.M) {
	//os.Setenv("MSG_API_BROKER", "AMQP")
	a = main.App{}
	a.Initialize()
	code := m.Run()
	log.Println("code", code)

	os.Exit(code)
}

func TestCreateMessageIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}
	payload := []byte(`{"service": "webex_teams","roomid": "test-room",
                        "messagetext": "Testing 123..."}`)

	req, _ := http.NewRequest("POST", "/api/v1/messages/", bytes.NewBuffer(payload))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestJSONValidatorIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}
	payload := []byte(`{"badrequest": "This is not right!"}`)

	req, _ := http.NewRequest("POST", "/api/v1/messages/", bytes.NewBuffer(payload))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusUnprocessableEntity, response.Code)
}

func TestCreateMessage(t *testing.T) {
	payload := []byte(`{"service": "webex_teams","roomid": "test-room",
                        "messagetext": "Testing 123..."}`)
	expected := main.NewMessage()
	json.Unmarshal(payload, &expected)
	ctrl := gomock.NewController(t)
	bc := main.NewMockBrokerClient(ctrl)
	bc.EXPECT().
		SendMessage(gomock.Any()).
		Do(func(m *main.Message) {
			id, err := uuid.FromString(m.Id)
			if err != nil {
				t.Fatal(err)
			}
			if id.Version() != 4 {
				t.Fatal("expected strategy id to be uuid version 4")
			}
			if expected.Service != m.Service {
				t.Fatalf("Service: got %s, want %s", m.Service, expected.Service)
			}
			if expected.RoomId != m.RoomId {
				t.Fatalf("RoomId: got %s, want %s", m.RoomId, expected.RoomId)
			}
			if expected.MessageText != m.MessageText {
				t.Fatalf("MessageText: got %s, want %s", m.MessageText, expected.MessageText)
			}
			if expected.MarkdownText != m.MarkdownText {
				t.Fatalf("MarkdownText: got %s, want %s", m.MarkdownText, expected.MarkdownText)
			}
		})
	a.Broker = bc

	req, _ := http.NewRequest("POST", "/api/v1/messages/", bytes.NewBuffer(payload))
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}
